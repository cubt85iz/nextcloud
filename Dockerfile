FROM nextcloud:25

RUN apt-get update && \
    apt-get install -y \
        ffmpeg \
        procps \
        smbclient \
        libmagickcore-6.q16-6-extra \
        tesseract-ocr \
        tesseract-ocr-eng && \
    rm -rf /var/lib/apt/lists/*